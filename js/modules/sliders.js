// Sliders

function sliders() {
	const sliderWelcomeButtons = document.querySelectorAll(
		'.welcome__slider_preview-button'
	);
	const sliderCertButtons = document.querySelectorAll(
		'.certificates__slider_preview-button'
	);

	const setSlide = (imgSelector, button, buttonsArr) => {
		const img = document.querySelector(imgSelector);
		const imgSrc = button.querySelector('img').src;

		if (imgSrc !== img.src) {
			img.style.opacity = '0';
			setTimeout(() => {
				img.style.opacity = '1';
				img.src = imgSrc;
			}, 100);
			buttonsArr.forEach((btn) => {
				btn.classList.remove('active');
			});
			button.classList.add('active');
		}
	};

	sliderWelcomeButtons.forEach((btn, i) => {
		btn.addEventListener('click', () => {
			setSlide('.welcome__slider_view-img', btn, sliderWelcomeButtons);
		});
		if (i === 0) {
			setSlide('.welcome__slider_view-img', btn, sliderWelcomeButtons);
		}
	});

	if (sliderCertButtons.length > 0) {
		sliderCertButtons.forEach((btn) => {
			btn.addEventListener('click', () => {
				setSlide(
					'.certificates__slider_view-img',
					btn,
					sliderCertButtons
				);
			});
		});
	}
}

export default sliders;
