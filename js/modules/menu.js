function menu() {
	const menuButton = document.querySelector('.mobile__menu-button');
	const menuImg = menuButton.querySelector('img');
	const menuLinks = document.querySelectorAll('.header__nav_list-item');
	const newParent = document.querySelector('.mobile');
	const navElement = document.querySelector('nav');

	const linkLogo = document.querySelector('.header__conntacts-logo');
	const order = document.querySelector('.header__order-calc');

	const toShow = () => {
		linkLogo.style.zIndex = '0';
		order.style.zIndex = '0';
		menuImg.src = '../../img/icons/close.svg';
		navElement.style.transform = 'translateX(+10px)';
	};

	const toClose = () => {
		linkLogo.style.zIndex = '5';
		order.style.zIndex = '5';
		menuImg.src = '../../img/icons/menu.svg';
		navElement.style.transform = `translateX(-${window.innerWidth}px)`;
	};

	newParent.append(navElement);

	menuButton.addEventListener('click', () => {
		if (newParent.classList.toggle('show')) {
			toShow();
		} else {
			toClose();
		}
	});

	menuLinks.forEach((link) => {
		link.addEventListener('click', () => {
			newParent.classList.toggle('show');
			toClose();
		});
	});
}

export default menu;
