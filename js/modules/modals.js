/* eslint-disable no-param-reassign */
function modals(url) {
	const modalBG = document.querySelector('.modal');
	const callLinks = document.querySelectorAll(
		'.header__conntacts-call .link'
	);
	const offerCards = document.querySelectorAll('.offers__carousel-card');
	const offerCardsContent = document.querySelectorAll(
		'.modal__offers_content p'
	);
	const closeButton = modalBG.querySelector('.modal__close-button img');
	const doneButton = modalBG.querySelector('.modal__recall_done .button');
	const windows = modalBG.querySelectorAll(
		'.modal__recall, .modal__recall_done, .modal__offers'
	);
	const [winRecall, winRecallDone, winOffer] = windows;
	const form = modalBG.querySelector('.modal__form');
	const inputs = modalBG.querySelectorAll('#u_name, #u_phone');

	// Check invalid inputs
	const checkValidInputs = () => {
		const result = [...inputs].some((input) => {
			if (!input.value) {
				input.classList.add('invalid');
				return true;
			}
			input.classList.remove('invalid');
			return false;
		});
		return !result;
	};

	// Show modals
	const showModal = (win) => {
		modalBG.classList.add('show');
		win.classList.add('show');
	};

	// Close modals
	const closeModal = () => {
		modalBG.classList.remove('show');
		windows.forEach((win) => {
			win.classList.remove('show');
		});
	};

	const showOffer = (title, desc, bgImg) => {
		winOffer.style.backgroundImage = `url(${bgImg})`;
		offerCardsContent[0].textContent = title;
		offerCardsContent[1].textContent = desc;
		showModal(winOffer);
	};

	// Offer Cards events
	offerCards.forEach((card) => {
		const onClick = (control) => {
			control.addEventListener('click', (event) => {
				event.preventDefault();
				const [title, , desc] = card.querySelectorAll(
					'.offers__carousel-card-desc p'
				);
				const img = card.querySelector('img').src;
				showOffer(title.textContent, desc.textContent, img);
			});
		};

		if (window.matchMedia('(min-width: 1330px)').matches) {
			const link = card.querySelector(
				'.offers__carousel-card-seemore.visible'
			);
			if (link) {
				onClick(link);
			}
		} else {
			onClick(card);
		}
	});

	// Input events
	inputs.forEach((input) => {
		input.addEventListener('input', () => {
			if (!input.value) {
				input.classList.add('invalid');
				input.placeholder = 'Вы не заполнили поле';
			} else {
				input.classList.remove('invalid');
			}
		});
	});

	// Close modals events
	[modalBG, closeButton, doneButton].forEach((btn) => {
		btn.addEventListener('click', (event) => {
			if (event.target === btn) {
				closeModal();
			}
		});
	});

	// Show Recall form
	callLinks.forEach((link) => {
		link.addEventListener('click', (event) => {
			event.preventDefault();
			showModal(winRecall);
		});
	});

	// Call send
	form.addEventListener('submit', (event) => {
		event.preventDefault();

		if (!checkValidInputs()) {
			return;
		}

		const request = new XMLHttpRequest();
		request.open('POST', url);
		// request.setRequestHeader('COntent-type', 'application/json')
		const formData = new FormData(form);

		// const obj = {};
		// formData.forEach((value, key) => {
		//     obj[key] = value;
		// });
		// const json = JSON.stringify(obj);

		request.send(formData);

		request.addEventListener('load', () => {
			if (request.status === 200) {
				form.reset();
				closeModal();
				showModal(winRecallDone);
			} else {
				closeModal();
			}
		});
	});
}

export default modals;
