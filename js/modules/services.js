// Services

function services() {
	const cards = document.querySelectorAll('.services__block');
	const cardsWrapper = document.querySelector('.services__wrapper');
	const images = [];

	let currentCard;

	const setImage = (card) => {
		const imgSrc = card.getAttribute('data-background');
		cardsWrapper.style.backgroundImage = `url("${imgSrc}")`;
	};

	const preLoadImages = () => {
		cards.forEach((card) => {
			const img = new Image();
			img.src = card.getAttribute('data-background');
			images.push(img);
		});
	};

	preLoadImages();

	cards.forEach((card) => {
		card.addEventListener('mouseover', () => {
			if (currentCard === card) {
				return;
			}
			currentCard = card;
			setImage(card);
		});
	});
}

export default services;
