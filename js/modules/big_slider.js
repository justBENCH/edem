/* eslint-disable no-shadow */
/* eslint-disable no-param-reassign */
// Big Slider

function bigSlider() {
	const bigslider = document.querySelector('.infrastructure__slider_container');

	if (!bigslider) {
		return;
	}

	const nextButton = document.querySelector('.nav_pages-right');
	const prevButton = document.querySelector('.nav_pages-left');
	const slideDisplay = document.querySelector('.nav_pages-number');
	const imgs = bigslider.querySelectorAll('.infrastructure__slider_container img');

	const sliderWidth = bigslider.offsetWidth;

	let totalSlides = 0;
	let currentSlide = 1;
	let currentX = 0;
	let wasCalc = false;

	const calcWidthSlider = () => {
		totalSlides = imgs.length;
		imgs.forEach((img) => {
			img.style.width = `${sliderWidth}px`;
		});

		bigslider.style.width = `${totalSlides * sliderWidth}px`;
		wasCalc = true;
	};

	const showNumber = (num) => {
		slideDisplay.textContent = `0${num}`;
	};

	const showSlide = (num) => {
		if (!wasCalc) calcWidthSlider();
		currentSlide = num;
		if (currentSlide > totalSlides) {
			currentSlide = 1;
		} else if (currentSlide === 0) {
			currentSlide = totalSlides;
		}

		currentX = sliderWidth * (currentSlide - 1);
		showNumber(currentSlide);
		bigslider.style.transform = `translateX(-${currentX}px)`;
	};

	nextButton.addEventListener('click', () => {
		showSlide(currentSlide + 1);
	});

	prevButton.addEventListener('click', () => {
		showSlide(currentSlide - 1);
	});

	showSlide(1);
}

export default bigSlider;
