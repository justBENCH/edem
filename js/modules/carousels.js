/* eslint-disable no-param-reassign */
// Carousel

function carousels() {
	const carouselElements = document.querySelectorAll('.carousel_wrapper');
	let isDown = false;
	let startX;
	let scrollLeft;

	const endScroll = (carousel) => {
		isDown = false;
		carousel.classList.remove('active');
	};

	const beginScroll = (e, carousel) => {
		isDown = true;
		carousel.classList.add('active');
		startX = e.pageX - carousel.offsetLeft;
		scrollLeft = carousel.scrollLeft;
	};

	const toScroll = (e, carousel) => {
		if (!isDown) return;
		e.preventDefault();
		const x = e.pageX - carousel.offsetLeft;
		const walk = x - startX;
		carousel.scrollLeft = scrollLeft - walk;
	};

	carouselElements.forEach((carousel) => {
		carousel.addEventListener('mousedown', (e) => {
			beginScroll(e, carousel);
		});

		carousel.addEventListener('mouseleave', () => {
			endScroll(carousel);
		});

		carousel.addEventListener('mouseup', () => {
			endScroll(carousel);
		});

		carousel.addEventListener('mousemove', (e) => {
			toScroll(e, carousel);
		});
	});
}

export default carousels;
