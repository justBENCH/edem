import sliders from './sliders';

function rooms(roomsData) {
	const roomParent = document.querySelector('.welcome__rooms_desc');

	const roomViewName = document.querySelector('.welcome__slider-name');
	const roomUrl = document.querySelector('.welcome__slider-details');

	const roomName = roomParent.querySelector('.h3');
	const roomDesc = roomParent.querySelector('.welcome__rooms_desc-text');
	const roomWrapper = roomParent.querySelector(
		'.welcome__rooms__desc_scrollbox'
	);

	const nextRoom = document.querySelector('.welcome__slider_buttons-right');
	const prevRoom = document.querySelector('.welcome__slider_buttons-left');

	let roomIndex = 1;

	const cards = [];

	const sliderPreview = document.querySelector('.slider_preview');

	if (sliderPreview.children.length !== 0) {
		return false;
	}

	let cardPrev;

	const changeSlider = (images) => {
		sliderPreview.innerHTML = images
			.map(
				(img) => `
					<div class="welcome__slider_preview-button slider_preview-button">
						<img class="slider_preview-img" src="${img}" alt="" />
					</div>
            `
			)
			.join('');
		sliders();
	};

	const switchRoom = (id) => {
		const card = cards[id];
		if (!card.classList.contains('active')) {
			if (cardPrev) {
				cardPrev.classList.remove('active');
			}
			const { name, images, description, url } = roomsData[id];
			card.classList.add('active');
			roomName.textContent = name;
			roomViewName.textContent = name;
			roomDesc.textContent = description;
			roomUrl.href = url;

			changeSlider(images);

			cardPrev = card;
		}
	};

	const appendRooms = () => {
		roomsData.forEach((data, i) => {
			const card = document.createElement('div');
			const { name, images } = data;
			card.classList.add('welcome__rooms__desc_scrollbox-item');

			card.innerHTML = `
                <img class="welcome__rooms__desc_scrollbox-item-img"
                    src="${images[0]}"
                    alt="${name}"
                />
                <div class="welcome__rooms__desc_scrollbox-item-name">
                    <p class="welcome__rooms__desc_scrollbox-item title">
                        Номер категории:
                    </p>
                    <p class="welcome__rooms__desc_scrollbox-item name">
                        ${name}
                    </p>
                </div>
            `;

			card.addEventListener('click', () => switchRoom(i));
			cards.push(card);
			if (i === 0) {
				switchRoom(0);
			}
			roomWrapper.append(card);
		});
	};

	appendRooms();

	nextRoom.addEventListener('click', () => {
		roomIndex += 1;
		if (roomIndex > cards.length) {
			roomIndex = 1;
		}
		switchRoom(roomIndex - 1);
	});

	prevRoom.addEventListener('click', () => {
		roomIndex -= 1;
		if (roomIndex === 0) {
			roomIndex = cards.length;
		}
		switchRoom(roomIndex - 1);
	});

	return true;
}

export default rooms;
