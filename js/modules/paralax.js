/* eslint-disable no-param-reassign */
function paralax() {
	if (!document.querySelector('section.massage')) {
		return;
	}

	const pixDivider = 15;

	const imgsUp = {};
	const imgsDown = {};

	const imgs = document.querySelectorAll('.massage__card_images-img');

	imgs.forEach((el) => {
		const sibNext = el.nextElementSibling;

		el.addEventListener('mouseover', (e) => {
			if (
				sibNext &&
				e.target.tagName === 'IMG' &&
				el.style.zIndex !== '1'
			) {
				sibNext.style.opacity = '0';
				setTimeout(() => {
					sibNext.style.opacity = '1';
					el.style.zIndex = '1';
				}, 200);
			}
		});

		el.addEventListener('mouseleave', () => {
			if (el.style.zIndex === '1') {
				el.style.opacity = '0';
				setTimeout(() => {
					el.style.opacity = '1';
					el.style.zIndex = '0';
				}, 200);
			}
		});
	});

	[('1_2', '3_2', '4_1')].forEach((el) => {
		const element = document.querySelector(`.card-img_${el}`);
		const top = element.getBoundingClientRect().top + window.scrollY;
		imgsUp[el] = {
			element,
			top,
		};
	});

	['1_1', '3_1', '4_2'].forEach((el) => {
		const element = document.querySelector(`.card-img_${el}`);
		const top = element.getBoundingClientRect().top + window.scrollY;
		imgsDown[el] = {
			element,
			top,
		};
	});

	function scrollImgs() {
		const scrollPos = window.scrollY;
		const toScroll = (img, imgTop, dowm = false) => {
			if (
				scrollPos >=
					imgTop - window.innerHeight - window.innerHeight / 3 &&
				scrollPos <=
					imgTop + window.innerHeight - window.innerHeight / 3
			) {
				if (!dowm) {
					img.style.transform = `translateY(${
						-(scrollPos - imgTop) / pixDivider
					}px)`;
				} else {
					img.style.transform = `translateY(${
						+(scrollPos - imgTop) / pixDivider
					}px)`;
				}
			}
		};

		Object.keys(imgsUp).forEach((img) => {
			toScroll(imgsUp[img].element, imgsUp[img].top);
		});

		Object.keys(imgsDown).forEach((img) => {
			toScroll(imgsDown[img].element, imgsDown[img].top, true);
		});
	}

	window.addEventListener('scroll', scrollImgs);
}

export default paralax;
