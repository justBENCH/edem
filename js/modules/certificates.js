function certificates() {
	const scrollList = document.querySelector('.certificates__slider_preview');

	if (!scrollList) {
		return;
	}

	scrollList.classList.replace('slider_preview', 'carousel_wrapper');

	scrollList.querySelectorAll('.slider_preview-button').forEach((btn) => {
		btn.classList.remove('slider_preview-button', 'active');
	});
}

export default certificates;
