// Smooth Scroll

function smoothScroll() {
	const smoothLinks = document.querySelectorAll('a[href^="#"]');

	smoothLinks.forEach((link) => {
		link.addEventListener('click', (e) => {
			e.preventDefault();
			const id = link.getAttribute('href');

			document.querySelector(id).scrollIntoView({
				behavior: 'smooth',
				block: 'start',
			});
		});
	});
}

export default smoothScroll;
