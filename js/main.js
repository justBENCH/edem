import 'core-js';

import bigSlider from './modules/big_slider';
import carousels from './modules/carousels';
import sliders from './modules/sliders';
import services from './modules/services';
import smoothScroll from './modules/smooth_scroll';
import menu from './modules/menu';
import certificates from './modules/certificates';
import modals from './modules/modals';
import paralax from './modules/paralax';
import rooms from './modules/rooms';

import configData from './config.json';

document.addEventListener('DOMContentLoaded', () => {
	carousels();
	if (!rooms(configData.rooms)) {
		sliders();
	}
	smoothScroll();
	bigSlider();
	modals(configData.recallUrl);

	if (window.matchMedia('(min-width: 1330px)').matches) {
		// Desktop
		services();
		paralax();
	} else if (window.matchMedia('(min-width: 810px)').matches) {
		// Laptop
		menu();
		certificates();
		paralax();
	} else {
		// Mobile
		menu();
		certificates();
	}
});
