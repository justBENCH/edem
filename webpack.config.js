'use strict';

let path = require('path');

module.exports = {
	// mode: 'development',
	mode: 'production',
	entry: './js/main.js',
	output: {
		filename: 'bundle.js',
		path: __dirname + '/js',
	},
	watch: true,

	devtool: 'cheap-source-map',

	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: [/\bcore-js\b/, /\bwebpack\/buildin\b/],
				use: {
					loader: 'babel-loader',
					options: {
						babelrc: false,
						configFile: path.resolve(__dirname, 'babel.config.js'),
						compact: false,
						cacheDirectory: true,
						sourceMaps: false,
					},
				},
			},
		],
	},
};
